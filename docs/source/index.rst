.. Placas de Vehiculos documentation master file, created by
   sphinx-quickstart on Thu Dec 17 19:48:33 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentación de la aplicación Placas de Vehículos.
===================================================

Aplicación de línea de comandos en Python que identifica si la placa de un vehículo es correcta y su tipo.

Requisitos:
Python 3

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: placavehiculos
   :members:
   :special-members:
   :undoc-members:
   
.. automodule:: test_placavehiculos
   :members:
   :special-members:
   :undoc-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
